//
//  CommonPort.swift
//  SNSBasePro
//
//  Created by zhan zhong yi on 2018/4/15.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import KeychainAccess

class CommonPort: NSObject {
    
    class func bundleId() -> String {
        let info = Bundle.main.infoDictionary;
        let bundleId = info!["CFBundleIdentifier"] as! String;
        return bundleId;
    }

    final class func keychain() -> Keychain {

        let keychain = Keychain(service: CommonPort.bundleId())
        return keychain;
    }
    
    class func set(value:String?,forPort key:String!){
        
        let keychain = self.keychain();
        keychain[key] = value;
    }
    
     class func value(forPort key: String) -> Any? {
        
        let keychain = self.keychain();
        return keychain[key];
    }
    
}
