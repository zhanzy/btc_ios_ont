//
//  AlamofireModel.swift
//  SNSBasePro
//
//  Created by zhan zhong yi on 2018/5/19.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import MBProgressHUD
import SwifterSwift

enum MethodType:Int {
    case get
    case post
}

//https://www.raywenderlich.com/188427/alamofire-tutorial-getting-started-3
var severUrl: String {
    get {
        if Const.url.kTDGeneralURL_nb_Ex.contains("app.weimobile.com/nearby") == false {
            return "http://" + Const.url.kTDGeneralURL_nb_Ex;
        }
        
        return  "https://" + Const.url.kTDGeneralURL_nb_Ex;
    } 
}


//关于封装 https://www.jianshu.com/p/f2bd2f7e1fce
//再次封装的demo https://github.com/LiPengYue/AlamofireMenager
class AlamofireModel: NSObject {
    
    var arrRes:[[String:AnyObject]]!;
    
    private static var _instance: AlamofireModel! = {
        let ins = AlamofireModel();
        return ins;
    }()
    
    
    public class func shared() -> AlamofireModel! {
        return _instance;
    }

    //MARK:common
    class func bReturnStatus(dict:Dictionary<String,Any>?) -> Bool {
        
        if dict == nil {
            print("error! dict is not dict!");
            return false;
        }
        
        let d:Dictionary<String,Any> = dict!
        var status = d["code"] as? String
        if (status != nil) && Int(status!)! == 0 {
            return true;
        }
        
        let st = d["code"] as? NSNumber
        if (st != nil) && st!.intValue == 0 {
            return true;
        }
        
        status = d["content"] as? String
        if (status != nil) && Int(status!)! > 0 {
            return true;
        }
        
        status = d["status"] as? String
        if (status != nil) && Int(status!)! > 0 {
            return true;
        }
        
        let myDict = d["body"] as? Dictionary<String,Any>;
        if myDict != nil {
            status = myDict!["status"] as? String
            if (status != nil) && Int(status!)! > 0 {
                return true;
            }
        }
        
        //用户未登录
        status = d["content"] as? String
        if (status != nil) && Int(status!)! == 1 {
            return true;
        }
        
        
        return false;
    }
    
    class func baseParams() -> Dictionary<String,Any> {
        return self.baseParams(bAZ: false);
    }
    
    class func baseParams(bAZ:Bool) -> Dictionary<String,Any> {
        
        let user = UserCenter.share();
        
        var d = Dictionary<String,Any>();
        let version = ZYUtilsSW.appVersion();
        let uniqdentifier = UIDevice.current.uniqueDeviceIdentifier();
        let appName = Bundle.main.infoDictionary!["CFBundleName"] as? String;
        let screenWidth = UIScreen.main.bounds.size.width;
        let screenHeight = UIScreen.main.bounds.size.height;
        let jealBreak = "0";

        let aid = Const.kAppID;
        let de = ZYUtilsSW.stringFromDate(Date(), formater: "yyyy-MM-dd HH:mm:ss")
        let ocd = String.init(format: "%@%@", aid,version);
        let cd = ocd.base64Encoded ?? "0000";
        let ua = String.init(format: "a|%@|%@|%@|%@|%@|%.0f|%.0f|%@|%@|%@|%@", UIDevice.current.deviceIOSVersion,appName!,version,appName!,uniqdentifier,screenWidth,screenHeight,jealBreak,aid,de,cd);

        d["_ua"] = ua.urlEncoded;
        d["device_id"] = uniqdentifier;
        d["user_id"] = user.uid;
        d["version"] = version;
        d["aid"] = aid;
        
        if bAZ == false {
            d["token"] =  user.token;
        }else {
            d["userId"] = user.uid;
            d["accesskey"] = user.accesskey;
            d["cd"] = cd;
            d["ver"] = version;
        }
        
        return d;
    }
    
    class func baseParamsDelete(_ params:Dictionary<String,Any>) -> Dictionary<String,Any> {
    
        var last = params;
        let delKeys = ["appver","version","device","Mod","Mos","Cd","de","ln","_ua"];
        for (key,_) in last {
            
            if delKeys.contains(key) {
                last[key] = nil;
            }
        }
        
        return last;
    }
    
    class func log<T>(_ response:DataResponse<T>,_ params:[String:String]) {
        
        let baseUrl = response.request?.url?.absoluteString;
        
        var fullUrl = "";
        var arr:[String]! = [String]();
        for (key,value) in params {
            let str = key + "=" + value;
            arr.append(str);
        }
        if(arr.count > 0){
            let last  = arr.joined(separator: "&");
            fullUrl = baseUrl! + "?" + last;
        }
        print("fullUrl:\(fullUrl)");
        print("url:\(String(describing: baseUrl))\n params:\(params.json)")
        
        let json = JSON(response.data!)
        print("json:\(json)")
    }
    
    //补足http
    class func severCheckUrl(_ url:String) -> String {
        
        var last = "";
        //如果开头没有http,就加上http
        if url.hasPrefix("http") == false {
            
            if url.contains("app.weimobile.com/nearby") == false {
                last = last + "http://" + url ;
                return last;
            }
            
            last = last + "https://" + url;
            return last;
        }
        
        return url;
    }
    
    class func getFullUrl(_ url:String,_ params:[String:String]) -> String{
        
        let lastParams = AlamofireModel.baseParamsDelete(params) as! [String:String];
        let baseUrl = url;
        
        var fullUrl = "";
        var arr:[String]! = [String]();
        for (key,value) in lastParams {
            let str = key + "=" + value;
            arr.append(str);
        }
        if(arr.count > 0){
            let last  = arr.joined(separator: "&");
            fullUrl = baseUrl + "?" + last;
        }
     
        return fullUrl;
    }
    
    //MARK: request action
    //use default url severUrl
    class func loadData(path: String,_ type : MethodType, parameters : [String : String]? = nil, inView:UIView? = nil, finishedCallback : @escaping (_ bSuccess:Bool, _ result : Any) -> ()) {
        return AlamofireModel.loadData(url: severUrl, path: path, type, parameters: parameters, inView: inView, finishedCallback: finishedCallback);
    }
    
    //manual url
    class func loadData(url:String,path: String,_ type : MethodType, parameters : [String : String]? = nil, inView:UIView? = nil, finishedCallback : @escaping (_ bSuccess:Bool, _ result : Any) -> ()) {
        
        // 1.获取类型
        let method = type == .get ? HTTPMethod.get : HTTPMethod.post
        
        var params = AlamofireModel.baseParams() as! [String:String];
        if parameters != nil {
            params = params.merged(with: parameters!)
        }
        
        var URL = AlamofireModel.severCheckUrl(url);
        if path.count > 0 {
            URL = URL + "/" + path;
        }
        // 2.发送网络请求
        if(inView != nil){
            MBProgressHUD.showAdded(to: inView!, animated: true);
        }
        Alamofire.request(URL, method: method, parameters: params).responseJSON { (response) in
            
            AlamofireModel.log(response,params);
            
            if(inView != nil){
                MBProgressHUD.hide(for: inView!, animated: false);
            }
            // 3.获取结果
            guard let result = response.result.value else {
                print(response.result.error!)
                var d = [String:String]();
                d["msg"] = Const.msg.kServerStateErrorEx;
                finishedCallback(false,d);
                return
            }
            
            let last = result as! Dictionary<String,Any>;
            if self.bReturnStatus(dict: last) == false {
                
                var d = [String:Any]();
                d["msg"] = last["msg"] as? String;
                if last["code"] is NSNumber {
                    d["code"] = (last["code"] as! NSNumber).stringValue;
                }else {
                   d["code"] = last["code"];
                }
                finishedCallback(false,d);
                return;
            }
            
            // 4.将结果回调出去
            finishedCallback(true,last)
        }
    }
    
    
     class func loadModel(path: String, _ type : MethodType, parameters : [String : String]? = nil, _ classType:AnyClass, inView:UIView? = nil,finishedCallback :  @escaping (_ bSuccess:Bool,_ result : Any) -> ()) {
        
        // 1.获取类型
        let method = type == .get ? HTTPMethod.get : HTTPMethod.post
        
        var params = AlamofireModel.baseParams() as! [String:String];
        if parameters != nil {
            params = params.merged(with: parameters!)
        }
        
        let url = severUrl;
        var URL = AlamofireModel.severCheckUrl(url);
        if path.count > 0 {
            URL = URL + "/" + path;
        }
        // 2.发送网络请求
        if(inView != nil){
            MBProgressHUD.showAdded(to: inView!, animated: true);
        }
        Alamofire.request(URL, method: method, parameters: params).responseJSON { (response) in
            
             AlamofireModel.log(response,params);
            if(inView != nil){
                MBProgressHUD.hide(for: inView!, animated: false);
            }
            // 3.获取结果
            guard let result = response.result.value else {
                print(response.result.error!)
                let mod = BaseAdapterMappable();
                mod.msg = Const.msg.kServerStateErrorEx;
                finishedCallback(false,mod);
                return
            }
            
            let swiftyJsonVar = JSON(result)
            if let resData = swiftyJsonVar["contacts"].arrayObject {
                let arr = resData as! [[String:AnyObject]]
                print(arr.debugDescription)
            }
            
            // 4.将结果回调出去
            finishedCallback(true,result)
        }
    }
    
    public func loadModelEx<T: Mappable>(path: String,_ type : HTTPMethod, parameters : [String : String]? = nil,inView:UIView? = nil, finishedCallback:  @escaping (_ bSuccess:Bool,_ response:DataResponse<T>) -> Void ) {
        
//        let locale = NSLocale.autoupdatingCurrent
//        let code = locale.languageCode!
//        let language = locale.localizedString(forLanguageCode: code)!
//        print("\(language)")
        
        var params = AlamofireModel.baseParams() as! [String:String];
        if parameters != nil {
            params = params.merged(with: parameters!)
        }

        if(inView != nil){
            MBProgressHUD.showAdded(to: inView!, animated: true);
        }
        let url = severUrl;
        var URL = AlamofireModel.severCheckUrl(url);
        if path.count > 0 {
            URL = URL + "/" + path;
        }
        _ = Alamofire.request(URL, method: .post,parameters:params).responseObject { (response: DataResponse<T>) in
            
//            print(response.request?.debugDescription);
//            print(response.response.debugDescription);
//            print(response.result.debugDescription);
        
            if(inView != nil){
                MBProgressHUD.hide(for: inView!, animated: false);
            }
            AlamofireModel.log(response,params);
            finishedCallback(true,response);
        }
    }


//    public class func responseObject<T: Mappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, mapToObject object: T? = nil, completionHandler: (DataResponse<T>) -> Void) -> Self {
//
//        let URL = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/d8bb95982be8a11a2308e779bb9a9707ebe42ede/sample_json"
//        Alamofire.request(URL).responseObject { (response: DataResponse<WeatherResponse>) in
//
//            let weatherResponse = response.result.value
//            print(weatherResponse?.location)
//
//            if let threeDayForecast = weatherResponse?.threeDayForecast {
//                for forecast in threeDayForecast {
//                    print(forecast.day)
//                    print(forecast.temperature)
//                }
//            }
//        }
//
//        return self;
//    }
//
//    public class func responseArray<T: Mappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, completionHandler: (DataResponse<[T]>) -> Void) -> Self{
//
//        let URL = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/f583be1121dbc5e9b0381b3017718a70c31054f7/sample_array_json"
//        Alamofire.request(URL).responseArray { (response: DataResponse<[Forecast]>) in
//
//            let forecastArray = response.result.value
//
//            if let forecastArray = forecastArray {
//                for forecast in forecastArray {
//                    print(forecast.day)
//                    print(forecast.temperature)
//                }
//            }
//        }
//
//        return self;
//    }
    
    //MARK: upload - common
    class func asURLRequest(_ path:String,params:[String:Any]) throws -> URLRequest {
        
        let url = severUrl;
        var URL = AlamofireModel.severCheckUrl(url);
        if path.count > 0 {
            URL = URL + "/" + path;
        }
        let urlEx = try (URL).asURL()
        
        var request = URLRequest(url: urlEx)
        request.httpMethod = HTTPMethod.post.rawValue;
//        request.setValue(Constants.authenticationToken, forHTTPHeaderField: "Authorization")
        request.timeoutInterval = TimeInterval(10 * 1000) //10s
        
        return try URLEncoding.default.encode(request, with: params)
    }
    
    //MARK: upload
     class func upload(path: String!,params:[String:String],inView:UIView? = nil,
                            progressCompletion: @escaping (_ percent: Float) -> Void,
                            completion: @escaping (_  bSuccess: Bool, _ results: Any?) -> Void) {
        
        let name = params["name"]!;
        let  mimetype = params["mimetype"]!;
        let filePath = params["filepath"]!;
        let parameters = params;
//        parameters["name"] = nil;
//        parameters["mimetype"] = nil;
//        parameters["filepath"] = nil;

        var baseParams = AlamofireModel.baseParams() as! [String:String];
        baseParams = baseParams.merged(with: parameters)
        
        let url = URL.init(fileURLWithPath: filePath);
        Alamofire.upload(multipartFormData: { multipartFormData in
            //            multipartFormData.append(url,
            //                                     withName: "imagefile",
            //                                     fileName: "image.jpg",
            //                                     mimeType: "image/jpeg")
            multipartFormData.append(url,
                                     withName: name,
                                     fileName: filePath,
                                     mimeType: mimetype)
            
            for (key,value) in baseParams {
                let data = value.data(using: String.Encoding.utf8);
                if data != nil {
                     multipartFormData.append(data!, withName: key);
                }
            }
        },
                         with:ImaggaRouter.createParams(baseParams),
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.uploadProgress { progress in
                                    progressCompletion(Float(progress.fractionCompleted))
                                }
                                upload.validate()
                                upload.responseJSON { response in
                                    guard response.result.isSuccess,
                                        let value = response.result.value else {
                                            print("Error while uploading file: \(String(describing: response.result.error))")
                                            let d = ["msg":Const.msg.kServerStateErrorEx,"des":String(describing: response.result.error)];
                                            let cb = JSON.init(d).rawValue;
                                            completion(false, cb);
                                            return
                                    }
                                    
//                                    let firstFileID = JSON(value)["uploaded"][0]["id"].stringValue
//                                    print("Content uploaded with ID: \(firstFileID)")
                                    
                                    //                                    self.downloadTags(contentID: firstFileID) { tags in
                                    //                                        self.downloadColors(contentID: firstFileID) { colors in
                                    //                                            completion(tags, colors)
                                    //                                        }
                                    //                                    }
                                    AlamofireModel.log(response,baseParams);
                                    //let cb = JSON(value).rawValue;
                                    completion(true,value);
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                                
                                let d = ["msg":Const.msg.kServerStateErrorEx,"des":encodingError.localizedDescription];
                                let cb = JSON.init(d).rawValue;
                                completion(false, cb);
                            }
        })
    }
    
    
    class func upload(image: UIImage,
                progressCompletion: @escaping (_ percent: Float) -> Void,
                completion: @escaping (_ tags: [String]?, _ colors: [PhotoColor]?) -> Void) {
        guard let imageData = UIImageJPEGRepresentation(image, 0.5) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData,
                                     withName: "imagefile",
                                     fileName: "image.jpg",
                                     mimeType: "image/jpeg")
        },
                         with: ImaggaRouter.content,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.uploadProgress { progress in
                                    progressCompletion(Float(progress.fractionCompleted))
                                }
                                upload.validate()
                                upload.responseJSON { response in
                                    guard response.result.isSuccess,
                                        let value = response.result.value else {
                                            print("Error while uploading file: \(String(describing: response.result.error))")
                                            completion(nil, nil)
                                            return
                                    }
                                    
                                    let firstFileID = JSON(value)["uploaded"][0]["id"].stringValue
                                    print("Content uploaded with ID: \(firstFileID)")
                                    
                                    self.downloadTags(contentID: firstFileID) { tags in
                                        self.downloadColors(contentID: firstFileID) { colors in
                                            completion(tags, colors)
                                        }
                                    }
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                            }
        })
    }
    
    class func downloadTags(contentID: String, completion: @escaping ([String]?) -> Void) {
        Alamofire.request(ImaggaRouter.tags(contentID))
            .responseJSON { response in
                guard response.result.isSuccess,
                    let value = response.result.value else {
                        print("Error while fetching tags: \(String(describing: response.result.error))")
                        completion(nil)
                        return
                }
                
                let tags = JSON(value)["results"][0]["tags"].array?.map { json -> String in
                    json["tag"].stringValue
                }
                
                completion(tags)
        }
    }
    
    class func downloadColors(contentID: String, completion: @escaping ([PhotoColor]?) -> Void) {
        Alamofire.request(ImaggaRouter.colors(contentID))
            .responseJSON { response in
                guard response.result.isSuccess,
                    let value = response.result.value else {
                        print("Error while fetching colors: \(String(describing: response.result.error))")
                        completion(nil)
                        return
                }
                
                let photoColors = JSON(value)["results"][0]["info"]["image_colors"].array?.map { json -> PhotoColor in
                    PhotoColor(red: json["r"].intValue,
                               green: json["g"].intValue,
                               blue: json["b"].intValue,
                               colorName: json["closest_palette_color"].stringValue)
                }
                
                completion(photoColors)
        }
    }
    
}

// MARK: - Response classes

// MARK: - ImmutableMappable

class ImmutableWeatherResponse: ImmutableMappable {
    let location: String
    let threeDayForecast: [ImmutableForecast]
    
    required init(map: Map) throws {
        location = try map.value("location")
        threeDayForecast = try map.value("three_day_forecast")
    }
    
    func mapping(map: Map) {
        location >>> map["location"]
        threeDayForecast >>> map["three_day_forecast"]
    }
}

class ImmutableForecast: ImmutableMappable {
    let day: String
    let temperature: Int
    let conditions: String
    
    required init(map: Map) throws {
        day = try map.value("day")
        temperature = try map.value("temperature")
        conditions = try map.value("conditions")
    }
    
    func mapping(map: Map) {
        day >>> map["day"]
        temperature >>> map["temperature"]
        conditions >>> map["conditions"]
    }
    
}

// MARK: - Mappable
class BaseAdapterMappable: Mappable {
    
    var itemId:String?;
    var code:NSNumber?;
    var status:String?;
    var msg:String?;
    var beginId:String?;
    var hasMore:NSNumber?;
    var results:Dictionary<String,Any>?;
    var ids:String?;
    
    init(){}
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        itemId <- map["id"]
        code <- map["code"]
        msg <- map["msg"]
        status <- map["status"]
        results <- map["results"]
        ids <- map["ids"]
        msg <- map["msg"]
    }
    
}

class BottleListResponse: BaseAdapterMappable {
    
    var bottles: [BottleResponse]?

    override init(){
        super.init();
    }
    
    required init?(map: Map){
        super.init(map: map);
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map);
        
        bottles <- map["bottles"]
    }
    
}

class BottleResponse: BaseAdapterMappable {
    
    enum BottleType:String {
        case none = "-1" //
        case common = "0" //文字瓶
        case image = "1" //预留
        case sound = "2" //语音瓶
        case xiehou = "3" //邂逅瓶
        
        static func initMy(str:String!) -> BottleType {
            return BottleType.init(rawValue: str)!
        }
    }
    
    var content:String?;
    var create:String?;
    var typeStr:String! = BottleType.none.rawValue;
    var user:MPUserModel?;
    var view_nums:String?;
    var viewers:Array<MPUserModel>?;
    var index:Int! = 0;
    var durationStr:String?;
    var duration:Int {
        
        set {
            durationStr = String.init(format: "%d", duration);
        }
        
        get {
            let str = durationStr ?? "0";
            if(self.type == BottleType.sound){
                guard let d = self.content?.convertToDictionary() else {
                    print("error! sound dictionary 转化失败!");
                    return 0;
                }
                
                guard let dur = d["duration"] as? String else {
                    print("warning! dictionary 中没有 duration值");
                    return  0;
                }
                
                durationStr = dur;
                return Int(durationStr!)!;
            }
            
            return Int(str)!;
        }
    }
    
    var type:BottleType! {
        get {
            return BottleType.init(rawValue: self.typeStr);
        }
        set {
            self.typeStr = newValue.rawValue;
        }
    }
    
    override init(){
        super.init();
    }
    
    required init?(map: Map){
        super.init(map: map);
    }
    
     override func mapping(map: Map) {
        super.mapping(map: map);
        
        content <- map["content"]
        create <- map["create_time"]
        typeStr <- map["type"]
        view_nums <- map["view_nums"]
        viewers <- map["scan_user_list"]
        durationStr <- map["duration"]
        user <- map["sender"]
    }
}

class WeatherResponse: Mappable {
    var location: String?
    var threeDayForecast: [Forecast]?
    var date: Date?
    
    init(){}
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        location <- map["location"]
        threeDayForecast <- map["three_day_forecast"]
    }
}

class Forecast: Mappable {
    var day: String?
    var temperature: Int?
    var conditions: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        day <- map["day"]
        temperature <- map["temperature"]
        conditions <- map["conditions"]
    }
}

struct WeatherResponseImmutable: ImmutableMappable {
    let location: String
    var threeDayForecast: [Forecast]
    var date: Date?
    
    init(map: Map) throws {
        location = try map.value("location")
        threeDayForecast = try map.value("three_day_forecast")
    }
    
    func mapping(map: Map) {
        location >>> map["location"]
        threeDayForecast >>> map["three_day_forecast"]
    }
}

struct ForecastImmutable: ImmutableMappable {
    let day: String
    var temperature: Int
    var conditions: String?
    
    init(map: Map) throws {
        day = try map.value("day")
        temperature = try map.value("temperature")
        conditions = try? map.value("conditions")
    }
    
    func mapping(map: Map) {
        day >>> map["day"]
        temperature >>> map["temperature"]
        conditions >>> map["conditions"]
    }
}

