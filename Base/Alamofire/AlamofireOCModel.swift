//
//  AlamofireModel.swift
//  SNSBasePro
//
//  Created by zhan zhong yi on 2018/5/19.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import Mantle
import TMCache

//https://www.raywenderlich.com/188427/alamofire-tutorial-getting-started-3
@objc enum MethodType:Int {
    case get
    case post
}

//关于封装 https://www.jianshu.com/p/f2bd2f7e1fce
//再次封装的demo https://github.com/LiPengYue/AlamofireMenager
class AlamofireOCModel: AlamofireModel {
    
    private static var _instance: AlamofireOCModel! = {
        let ins = AlamofireOCModel();
        return ins;
    }()
    
    override class func shared() -> AlamofireOCModel {
        return _instance;
    }
    
    //MARK:Common
//    class func classFromString(_ className: String) -> AnyClass! {
//
//        /// get namespace
//        let namespace = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String;
//
//        /// get 'anyClass' with classname and namespace
//        //let name = namespace + className;
//        let cls: AnyClass = NSClassFromString(className)!;
//
//        // return AnyClass!
//        return cls;
//    }

    //MARK: action
    @objc class func loadCache(_ path:String, _ parameters:Dictionary<String,String>,handler:@escaping(_ bSuccess:Bool, _ callBacks:Any?) -> ()) {
        
        DispatchQueue.main.async {
            
            let url = severUrl + path;
            var params = AlamofireModel.baseParams() as! [String:String];
             params = params.merged(with: parameters)
            
            let key = AlamofireModel.getFullUrl(url, params);
            let object = TMCache.shared().object(forKey: key)
            if(object == nil){
                handler(false,nil);
                return;
            }
            
            handler(true,object);
        }
    }
    
    @objc class func loadOCModel(path: String, _ type : MethodType, _ parameters : [String : String]? = nil, _ classType:AnyClass, finishedCallback :  @escaping (_ bSuccess:Bool, _ result : Any?) -> ()) {
        return self.loadOCModel(path: path, type, parameters, classType, bCache: false, finishedCallback: finishedCallback);
    }
    
    @objc class func loadOCModel(path: String, _ type : MethodType, _ parameters : [String : String]? = nil, _ classType:AnyClass, bCache : Bool = false,  finishedCallback :  @escaping (_ bSuccess:Bool, _ result : Any?) -> ()) {
        
        // 1.获取类型
        let method = type == .get ? HTTPMethod.get : HTTPMethod.post
        
        let URL = severUrl + path;
        var params = AlamofireModel.baseParams() as! [String:String];
        if parameters != nil {
            params = params.merged(with: parameters!)
        }
        
        // 2.发送网络请求
        Alamofire.request(URL, method: method, parameters: params).responseJSON { (response) in
            
            AlamofireModel.log(response, parameters!);
            
            // 3.获取结果
            guard let result = response.result.value else {
                print(response.result.error!)
                let mod = BaseModel();
                mod!.msg = Const.msg.kServerStateErrorEx;
                finishedCallback(false,mod);
                return
            }
            
//            let swiftyJsonVar = JSON(result)
            let d = result as! Dictionary<String,Any>;
            var mod:Any?;
            do {

                mod = try MTLJSONAdapter.model(of: classType.self, fromJSONDictionary: d, error: ())
                if(mod != nil){
                    if(bCache){
                        //预留,缓存数据
                        let url  = response.request?.url?.absoluteString;
                        let key = AlamofireModel.getFullUrl(url!, params);
                        TMCache.shared().setObject(mod as! NSCoding, forKey: key);
                        
//                        let data = TMCache.shared().object(forKey: key);
//                        if data != nil {
//                            print("success data:\(data.debugDescription)");
//                        }
                    }
                    finishedCallback(true,mod);
                }else {
                    let mod = BaseModel();
                    mod!.msg = Const.msg.kReturnStatusErrorEx;
                    finishedCallback(false,mod);
                }
                
            } catch {
                // error异常的对象
                print(error.localizedDescription)
                let mod = BaseModel();
                mod!.msg = Const.msg.kReturnStatusErrorEx;
                finishedCallback(false,mod);
            }
        }
    }
}


