//
//  AppController.swift
//  Wallet
//
//  Created by Kishikawa Katsumi on 2018/02/05.
//  Copyright © 2018 Kishikawa Katsumi. All rights reserved.
//

import Foundation
import BitcoinKit
import KeychainAccess

class AppController {
    static let shared = AppController()
    let network = Network.testnet
    var wallet:HDWallet?
    var mnemonic:[String]!;

    private(set) var wallets = [HDWallet]()
    
    //获取钱包
    public func getWallet() -> HDWallet? {
        
        if wallet != nil {
            return wallet;
        }
        
        createWallet();
        return wallet;
    }
    
    func createWallet() {
        
        do {
            self.mnemonic = try Mnemonic.generate()
            print("seed key:\(self.mnemonic)")
//                        for (mnemonic, label) in zip(mnemonic, smnemonicLabels) {
//                            label.text = mnemonic
//                        }
        } catch {
            let alert = UIAlertController(title: "Crypto Error", message: "Failed to generate random seed. Please try again later.", preferredStyle: .alert)
            AppDelegate.share().window!.rootViewController?.present(alert, animated: true, completion: nil)
            return;
        }
        
        let seed = Mnemonic.seed(mnemonic: self.mnemonic)
        let wallet = HDWallet(seed: seed, network: AppController.shared.network)
        AppController.shared.addWallet(wallet)
    }

    func addWallet(_ wallet: HDWallet) {
        wallets.append(wallet)
        self.wallet = wallet;
        
        if let serialized = try? JSONEncoder().encode(wallets) {
            let keychain = Keychain()
            keychain[data: "wallets"] = serialized
        }

        NotificationCenter.default.post(name: Notification.Name.AppController.walletChanged, object: self)
    }

    private init() {
        let keychain = Keychain()
        if let serialized = keychain[data: "wallets"], let wallets = try? JSONDecoder().decode([HDWallet].self, from: serialized) {
            self.wallets = wallets
        }
    }
}

extension Notification.Name {
    struct AppController {
        static let walletChanged = Notification.Name("AppController.walletChanged")
    }
}
