//
//  AppDelegate.swift
//  Base
//
//  Created by PC-269 on 2018/7/27.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var viewConroller:ViewController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "ViewController")
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
        self.viewConroller? = initialViewController as! ViewController;
        
        let _ = SmokeTest();
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //self.serverCheck();
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: - common
    class func  share() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate;
    }
    
    @objc public func isAllowExpand() -> Bool{
        
        let b = self.bCheckVersion();
        return b;
    }
    
    func bCheckVersion() -> Bool {
        
        let key = self.checkVersionKey();
        let b = UserDefaults.standard.string(forKey: key)
        if b == nil {
            return false;
        }
        
        let version =  ZYUtilsSW.appVersion();
        if b!.compare(version) == ComparisonResult.orderedAscending {
            return false;
        }
        
        return true;
    }
    
    func checkVersionKey() -> String {
        
        let infoDictionary = Bundle.main.infoDictionary;
        let bunle = infoDictionary!["CFBundleIdentifier"] as! String;
        let key = bunle.appendingFormat(".%@", "checkVer")
        return key;
    }
    
    func serverCheck() {
        
        if self.bCheckVersion() == true {
            print("check ok")
            return;
        }
        
        let url = Const.url.kTDGeneralURLEx;
        let action =  "m_mobileapp!checkAppVer.action";
        var params = ["action":action];
        params["mobileAppKey"] = Const.kAppID;
        params["mobileAppVer"] = ZYUtilsSW.appVersion();
        AlamofireModel.loadData(url:url,path: action, .post, parameters: params, inView: nil) { (bSuccess, callBacks) in
            
            //        1 没有找到应用
            //        2 参数异常，
            //        3 是启动 ，
            //        4 是未启动。
            //print("now:\(String(describing: callBacks))");
            guard let d = callBacks as? Dictionary<String,Any> else {
                print("获取失败")
                return;
            }
            
            if AlamofireModel.bReturnStatus(dict: d) == false {
                print("版本验证失败！");
                return;
            }
            
            let content = d["content"] as? String
            if content != nil  && Int(content!) == 3 {
                
                let key = self.checkVersionKey();
                let version =  ZYUtilsSW.appVersion();
                UserDefaults.standard.set(version, forKey: key);

//               获取一下apps
//                AppDelegate1.share().getRecommendApps();
                NotificationCenter.default.post(name: NSNotification.Name.init(NotificationNames.kServerCheckSuccess), object: nil)
            }
        }
    }

}

