//
//  UserMapModel.swift
//  SNSBasePro
//
//  Created by zhan zhong yi on 2018/5/21.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import ObjectMapper

class MPUserModel: BaseAdapterMappable {
    
    var _name:String?
    var name:String? {
        get {
            if(_name != nil){
                return _name;
            }
            
            if(self.nick_name != nil){
                return self.nick_name;
            }

            return "";
        }
        set {
            _name = newValue;
        }
    };
    var nick_name:String?;
    var content:String?;
    
    override init(){
        super.init();
    }
    
    required init?(map: Map){
        super.init(map: map);
    }
    
    
    override func mapping(map: Map) {
        super.mapping(map: map);
        
        name <- map["name"]
        nick_name <- map["nick_name"]
        
    }
}
