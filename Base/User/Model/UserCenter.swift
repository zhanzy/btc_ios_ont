//
//  UserCenter.swift
//  Base
//
//  Created by zhan zhong yi on 2018/7/30.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

class UserCenter: BaseAdapterMappable {

    static let _instance:UserCenter  = UserCenter();
    var uid:String?;
    var token:String?;
    var accesskey:String?;

    class func share() -> UserCenter {
        return _instance;
    }
}
