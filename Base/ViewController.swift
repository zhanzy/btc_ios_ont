//
//  ViewController.swift
//  Base
//
//  Created by PC-269 on 2018/7/27.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import KeychainAccess
import Alamofire
import SwiftyJSON
//import BitcoinKit

class ViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let keychain = Keychain(service: "com.example.ont")
        keychain["firest"] = "01234567-89ab-cdef-0123-456789abcdef"
        
        let car = MainController.init(nibName: MainController.className, bundle: nil)
        let nav = UINavigationController.init(rootViewController: car)

        let car1 = TransferController.init(nibName: TransferController.className, bundle: nil)
        let nav1 = UINavigationController.init(rootViewController: car1)
        
        self.viewControllers = [nav,nav1]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

