//
//  ScryptPlugin.swift
//  Base
//
//  Created by PC-269 on 2018/8/9.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

//https://www.huweihuang.com/article/blockchain/blockchain-keys&addresses/
class OntSdk: NSObject {
    
    private var walletMgr:WalletMgr?;
    private var connRpc:ConnectMgr?;
    private var connRestful:ConnectMgr?;
    private var connWebSocket:ConnectMgr?;
    private var connDefault:ConnectMgr?;
    
//        private Vm vm = null;
//        private NativeVm nativevm = null;
//        private NeoVm neovm = null;
//        private WasmVm wasmvm = null;
//        private SignServer signServer = null;
//
//        private static OntSdk instance = null;
//        public SignatureScheme defaultSignScheme = SignatureScheme.SHA256WITHECDSA;
//        public long DEFAULT_GAS_LIMIT = 30000;

    private static var _instance: OntSdk! = {
        let ins = OntSdk();
        return ins;
    }()
    
    public class func getInstance() -> OntSdk! {
        return _instance;
    }
    
    override init() {
        super.init();
    }
    
    public func getRestful() -> ConnectMgr {
        if connRestful == nil {
            //assert(true, ErrorCode.ConnRestfulNotInit)
        }
        
        return connRestful!;
    }

    public func setRestful(_ url:String) {
        self.connRestful = ConnectMgr(url,"restful")
    }
    
    public func getConnect() -> ConnectMgr? {
        
        if connDefault != nil {
            return connDefault;
        }
        
        if connRpc != nil {
            return connRpc;
        }

        if connRestful != nil {
            return connRestful;
        }
        
        if connWebSocket != nil {
            return connWebSocket;
        }
        
        return nil;
    }
    
    public func setDefaultConnect(_ conn:ConnectMgr) {
        connDefault = conn;
    }
    
    //MARK: - Wallet
    public func getWalletMgr() -> WalletMgr? {
        return walletMgr;
    }
    
//    public SignServer getSignServer() throws SDKException{
//    if(signServer == null){
//    throw new SDKException(ErrorCode.OtherError("signServer null"));
//    }
//    return signServer;
//    }
//    public NativeVm nativevm() throws SDKException{
//    if(nativevm == null){
//    vm();
//    nativevm = new NativeVm(getInstance());
//    }
//    return nativevm;
//    }
//
//    public NeoVm neovm() {
//    if (neovm == null) {
//    vm();
//    neovm = new NeoVm(getInstance());
//    }
//    return neovm;
//    }
//
//    public WasmVm wasmvm() {
//    if (wasmvm == null) {
//    vm();
//    wasmvm = new WasmVm(getInstance());
//    }
//    return wasmvm;
//    }
//
//    public Vm vm() {
//    if (vm == null) {
//    vm = new Vm(getInstance());
//    }
//    return vm;
//    }
//
//    public ConnectMgr getRpc() throws SDKException {
//    if (connRpc == null) {
//    throw new SDKException(ErrorCode.ConnRestfulNotInit);
//    }
//    return connRpc;
//    }
//
//    public ConnectMgr getRestful() throws SDKException {
//    if (connRestful == null) {
//    throw new SDKException(ErrorCode.ConnRestfulNotInit);
//    }
//    return connRestful;
//    }
//
//    public ConnectMgr getConnect() {
//    if (connDefault != null) {
//    return connDefault;
//    }
//    if (connRpc != null) {
//    return connRpc;
//    }
//    if (connRestful != null) {
//    return connRestful;
//    }
//    if (connWebSocket != null) {
//    return connWebSocket;
//    }
//    return null;
//    }
//
//    public void setDefaultConnect(ConnectMgr conn) {
//    connDefault = conn;
//    }
//
//    public ConnectMgr getWebSocket() throws SDKException {
//    if (connWebSocket == null) {
//    throw new SDKException(ErrorCode.WebsocketNotInit);
//    }
//    return connWebSocket;
//    }
//
//
//    /**
//     * get Wallet Mgr
//     *
//     * @return
//     */
//    public WalletMgr getWalletMgr() {
//    return walletMgr;
//    }
//
//
//    /**
//     * @param scheme
//     */
//    public void setSignatureScheme(SignatureScheme scheme) {
//    defaultSignScheme = scheme;
//    walletMgr.setSignatureScheme(scheme);
//    }
//
//    public void setSignServer(String url) throws Exception{
//    this.signServer = new SignServer(url);
//    }
//    public void setRpc(String url) throws MalformedURLException {
//    this.connRpc = new ConnectMgr(url, "rpc");
//    }
//
//    public void setRestful(String url) throws MalformedURLException {
//    this.connRestful = new ConnectMgr(url, "restful");
//    }
//
//    public void setWesocket(String url, Object lock) {
//    connWebSocket = new ConnectMgr(url, "websocket", lock);
//    }
//
//    /**
//     * @param path
//     */
//    public void openWalletFile(String path) {
//
//    try {
//    this.walletMgr = new WalletMgr(path,defaultSignScheme);
//    setSignatureScheme(defaultSignScheme);
//    } catch (Exception e) {
//    e.printStackTrace();
//    }
//    }
//
//    public void openWalletFile(SharedPreferences sp) throws IOException {
//
//    this.walletMgr = new WalletMgr(sp, defaultSignScheme);
//    setSignatureScheme(defaultSignScheme);
//    }
//
//    /**
//     * @param tx
//     * @param addr
//     * @param password
//     * @return
//     * @throws Exception
//     */
//    public Transaction addSign(Transaction tx, String addr, String password,byte[] salt) throws Exception {
//    return addSign(tx, getWalletMgr().getAccount(addr, password,salt));
//    }
//
//    public Transaction addSign(Transaction tx, Account acct) throws Exception {
//    if (tx.sigs == null) {
//    tx.sigs = new Sig[0];
//    }
//    Sig[] sigs = new Sig[tx.sigs.length + 1];
//    for (int i = 0; i < tx.sigs.length; i++) {
//    sigs[i] = tx.sigs[i];
//    }
//    sigs[tx.sigs.length] = new Sig();
//    sigs[tx.sigs.length].M = 1;
//    sigs[tx.sigs.length].pubKeys = new byte[1][];
//    sigs[tx.sigs.length].sigData = new byte[1][];
//    sigs[tx.sigs.length].pubKeys[0] = acct.serializePublicKey();
//    sigs[tx.sigs.length].sigData[0] = tx.sign(acct,defaultSignScheme);
//    tx.sigs = sigs;
//    return tx;
//    }
//
//    public Transaction addMultiSign(Transaction tx, int M, Account[] acct) throws Exception {
//    if (tx.sigs == null) {
//    tx.sigs = new Sig[0];
//    }
//    Sig[] sigs = new Sig[tx.sigs.length + 1];
//    for (int i = 0; i < tx.sigs.length; i++) {
//    sigs[i] = tx.sigs[i];
//    }
//    sigs[tx.sigs.length] = new Sig();
//    sigs[tx.sigs.length].M = M;
//    sigs[tx.sigs.length].pubKeys = new byte[acct.length][];
//    sigs[tx.sigs.length].sigData = new byte[acct.length][];
//    for (int i = 0; i < acct.length; i++) {
//    sigs[tx.sigs.length].pubKeys[i] = acct[i].serializePublicKey();
//    sigs[tx.sigs.length].sigData[i] = tx.sign(acct[i], acct[i].getSignatureScheme());
//    }
//    tx.sigs = sigs;
//    return tx;
//    }
//    public Transaction addMultiSign(Transaction tx,int M,byte[][] pubKeys, Account acct) throws Exception {
//    addMultiSign(tx,M,pubKeys,tx.sign(acct, acct.getSignatureScheme()));
//    return tx;
//    }
//    public Transaction addMultiSign(Transaction tx,int M,byte[][] pubKeys, byte[] signatureData) throws Exception {
//    pubKeys = Program.sortPublicKeys(pubKeys);
//    if (tx.sigs == null) {
//    tx.sigs = new Sig[0];
//    } else {
//    if (tx.sigs.length  > Common.TX_MAX_SIG_SIZE || M > pubKeys.length || M <= 0 || signatureData == null || pubKeys == null) {
//    throw new SDKException(ErrorCode.ParamError);
//    }
//    for (int i = 0; i < tx.sigs.length; i++) {
//    if(Arrays.deepEquals(tx.sigs[i].pubKeys,pubKeys)){
//    if (tx.sigs[i].sigData.length + 1 > pubKeys.length) {
//    throw new SDKException(ErrorCode.ParamErr("too more sigData"));
//    }
//    if(tx.sigs[i].M != M){
//    throw new SDKException(ErrorCode.ParamErr("M error"));
//    }
//    int len = tx.sigs[i].sigData.length;
//    byte[][] sigData = new byte[len+1][];
//    for (int j = 0; j < tx.sigs[i].sigData.length; j++) {
//    sigData[j] = tx.sigs[i].sigData[j];
//    }
//    sigData[len] = signatureData;
//    tx.sigs[i].sigData = sigData;
//    return tx;
//    }
//    }
//    }
//    Sig[] sigs = new Sig[tx.sigs.length + 1];
//    for (int i = 0; i < tx.sigs.length; i++) {
//    sigs[i] = tx.sigs[i];
//    }
//    sigs[tx.sigs.length] = new Sig();
//    sigs[tx.sigs.length].M = M;
//    sigs[tx.sigs.length].pubKeys = pubKeys;
//    sigs[tx.sigs.length].sigData = new byte[1][];
//    sigs[tx.sigs.length].sigData[0] = signatureData;
//
//    tx.sigs = sigs;
//    return tx;
//    }
//    public Transaction signTx(Transaction tx, String address, String password,byte[] salt) throws Exception {
//    address = address.replace(Common.didont, "");
//    signTx(tx, new Account[][]{{getWalletMgr().getAccount(address, password,salt)}});
//    return tx;
//    }
//
//    /**
//     * sign tx
//     *
//     * @param tx
//     * @param accounts
//     * @return
//     */
//    public Transaction signTx(Transaction tx, Account[][] accounts) throws Exception {
//    Sig[] sigs = new Sig[accounts.length];
//    for (int i = 0; i < accounts.length; i++) {
//    sigs[i] = new Sig();
//    sigs[i].pubKeys = new byte[accounts[i].length][];
//    sigs[i].sigData = new byte[accounts[i].length][];
//    for (int j = 0; j < accounts[i].length; j++) {
//    sigs[i].M++;
//    byte[] signature = tx.sign(accounts[i][j], defaultSignScheme);
//    sigs[i].pubKeys[j] = accounts[i][j].serializePublicKey();
//    sigs[i].sigData[j] = signature;
//    }
//    }
//    tx.sigs = sigs;
//    return tx;
//    }
//
//    /**
//     * signTx
//     *
//     * @param tx
//     * @param accounts
//     * @param M
//     * @return
//     * @throws SDKException
//     */
//    public Transaction signTx(Transaction tx, Account[][] accounts, int[] M) throws Exception {
//    if (M.length != accounts.length) {
//    throw new SDKException(ErrorCode.ParamError);
//    }
//    tx = signTx(tx, accounts);
//    for (int i = 0; i < tx.sigs.length; i++) {
//    if (M[i] > tx.sigs[i].pubKeys.length || M[i] < 0) {
//    throw new SDKException(ErrorCode.ParamError);
//    }
//    tx.sigs[i].M = M[i];
//    }
//    return tx;
//    }
//
//    public byte[] signatureData(com.github.ontio.account.Account acct, byte[] data) throws SDKException {
//    DataSignature sign = null;
//    try {
//    data = Digest.sha256(Digest.sha256(data));
//    sign = new DataSignature(defaultSignScheme, acct, data);
//    return sign.signature();
//    } catch (Exception e) {
//    throw new SDKException(e);
//    }
//    }
//
//    public boolean verifySignature(byte[] pubkey, byte[] data, byte[] signature) throws SDKException {
//    DataSignature sign = null;
//    try {
//    sign = new DataSignature();
//    data = Digest.sha256(Digest.sha256(data));
//    return sign.verifySignature(new com.github.ontio.account.Account(false, pubkey), data, signature);
//    } catch (Exception e) {
//    throw new SDKException(e);
//    }
//    }
    
    
}
