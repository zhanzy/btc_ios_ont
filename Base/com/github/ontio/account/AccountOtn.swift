//
//  Account.swift
//  Base
//
//  Created by PC-269 on 2018/8/10.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import ObjectMapper
import BitcoinKit
import EllipticCurveKeyPair

class AccountOtn : NSObject {

    private var keyType:Int = KeyType.ECDSA;
    private var curveParams:[String] = [String]();
    private var publicKey:BitcoinKit.HDPublicKey?;
    private var privateKey:BitcoinKit.HDPrivateKey?;
    private var addressU160:String?;
    private var signatureScheme:SignatureScheme?;
//    private var gen:KeyPair;
    
    public func getSignatureScheme() -> SignatureScheme?{
        return signatureScheme;
    }
    
    //https://github.com/ontio-community/specifications/blob/master/sdk_dev_standard/cn/account.md
    init(_ scheme:SignatureScheme) {
        super.init();
        
//        KeyPairGenerator gen;
        var paramSpec:String = "";
        signatureScheme = scheme;

        if (scheme.name == SignatureScheme.SHA256WITHECDSA) {
            self.keyType = KeyType.ECDSA;
            self.curveParams = [Curve.P256.toString()]
        } else if (scheme.name == SignatureScheme.SM3WITHSM2) {
            self.keyType = KeyType.SM2;
            self.curveParams = [Curve.SM2P256V1.toString()];
        }
        
        if(scheme.name == SignatureScheme.SHA256WITHECDSA || scheme.name == SignatureScheme.SM3WITHSM2 ){
            
            if (curveParams[0].isEmpty == true) {
                assert(true, "error! curveParams is empty!");
            }
            let curveName = curveParams[0];
            paramSpec = curveName;
//            gen = KeyPairGenerator.getInstance("EC", "SC");
        }else {
            assert(true, "error!\(ErrorCode.UnsupportedKeyType)")
        }
        
        //just do for ecdsa code
        self.privateKey = AppController.shared.getWallet()?.privateKey;
        self.publicKey = AppController.shared.getWallet()?.publicKey;

        
        //获取钱包地址
        self.addressU160 = self.publicKey?.toAddress();
        print("address160:\(self.addressU160)")
    }

    
    //MARK: - common
    
}
