//
//  Address.swift
//  Base
//
//  Created by PC-269 on 2018/8/14.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import BitcoinKit

class Address: UIntBase {

    public static var COIN_VERSION = 0x17;

    //[UInt8] == Data
    init(_ value:Data) {
        super.init(20, value);
    }
    
    public class func addressFromPubKey(_ publicKey:Data) -> Address {
    
//    ScriptBuilder sb = new ScriptBuilder();
//    sb.emitPushByteArray(publicKey);
//    sb.add(ScriptOp.OP_CHECKSIG);
//    return new Address(Digest.hash160(sb.toArray()));
        let d = BitcoinKit.Crypto.sha256ripemd160(publicKey)
        return Address(d)
    }
}
