//
//  ErrorCode.swift
//  Base
//
//  Created by PC-269 on 2018/8/13.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

class ErrorCode: NSObject {

    
    public static func getError(_ code:Int,_ msg:String) -> String{
        var d = Dictionary<String,String>();
        d["Error"] = "\(code)";
        d["Desc"] = msg;
        return d.json;
    }
    //account error
    public static let InvalidParams = getError(51001,"Account Error,invalid params");
    public static func InvalidParams(_ msg:String ) -> String { return getError(51001,msg);};
    public static let UnsupportedKeyType = getError(51002,"Account Error,unsupported key type");
    public static let InvalidMessage = getError(51003,"Account Error,invalid message");
    public static let WithoutPrivate = getError(51004,"Account Error,account without private key cannot generate signature");
    public static let InvalidSM2Signature = getError(51005,"Account Error,invalid SM2 signature parameter, ID (String) excepted");
    public static let AccountInvalidInput = getError(51006,"Account Error,account invalid input");
    public static let AccountWithoutPublicKey = getError(51007,"Account Error,account without public key cannot verify signature");
    public static let UnknownKeyType = getError(51008,"Account Error,unknown key type");
    public static let NullInput = getError(51009,"Account Error,null input");
    public static let InvalidData = getError(51010,"Account Error,invalid data");
    public static let Decoded3bytesError = getError(51011,"Account Error,decoded 3 bytes error");
    public static let DecodePrikeyPassphraseError = getError(51012,"Account Error,decode prikey passphrase error.");
    public static let PrikeyLengthError = getError(51013,"Account Error,Prikey length error");
    public static let PrefixNotMatch = getError(51014,"Key Password and Prefix not match");
    public static let NotSetCodeAddress = getError(51015, "Not Set Code Address");
    public static let KeyAddressPwdNotMatch = getError(51015, "Key Address and Password Not Match");
    public static let EncryptedPriKeyError = getError(51014,"Account Error,Prikey length error");
    public static let EncriptPrivateKeyError = getError(51016, "Account Error, encript privatekey error,");
    public static let encryptedPriKeyAddressPasswordErr = getError(51015, "Account Error,encryptedPriKey address password not match.");
    
    public static let InputError = getError(52001,"Uint256 Error,input error");
    public static let ChecksumNotValidate = getError(52002,"Base58 Error,Checksum does not validate");
    public static let InputTooShort = getError(52003,"Base58 Error,Input too short");
    public static let UnknownCurve = getError(52004,"Curve Error,unknown curve");
    public static let UnknownCurveLabel = getError(52005,"Curve Error,unknown curve label");
    public static let UnknownAsymmetricKeyType = getError(52006,"keyType Error,unknown asymmetric key type");
    public static let InvalidSignatureData = getError(52007,"Signature Error,invalid signature data: missing the ID parameter for SM3withSM2");
    public static let InvalidSignatureDataLen = getError(52008,"Signature Error,invalid signature data length");
    public static let MalformedSignature = getError(52009,"Signature Error,malformed signature");
    public static let UnsupportedSignatureScheme = getError(52010,"Signature Error,unsupported signature scheme:");
    public static let VerifyOntIdClaimErr = getError(58014,"OntIdTx Error, verifyOntIdClaim error");
    
    
    //transaction
    public static let TxDeserializeError = getError(53001,"Core Error,Transaction deserialize failed");
    public static let BlockDeserializeError = getError(53002,"Core Error,Block deserialize failed");
    
    
    //merkle error
    public static let MerkleVerifierErr = getError(54001, "Wrong params: the tree size is smaller than the leaf index");
    public static let TargetHashesErr = getError(54002, "targetHashes error");
    
    public static func ParamErr(_ msg:String) -> String {
    return getError(58005,msg);
    }
    
    public static let AsserFailedHashFullTree = getError(54004, "assert failed in hash full tree");
    public static let LeftTreeFull = getError(54005, "left tree always full");
    
    
    //SmartCodeTx Error
    public static let SendRawTxError = getError(58001, "SmartCodeTx Error,sendRawTransaction error");
    public static let TypeError = getError(58002, "SmartCodeTx Error,type error");
    
    
    public static let AmountError = getError(58105,"OntAsset Error,amount is less than or equal to zero");
    public static let WriteVarBytesError = getError(58015,"OntIdTx Error, writeVarBytes error");
    
    public static let ExpireErr = getError(58017,"OntIdTx Error, expire is wrong");
    public static let NullCodeHash = getError(58003,"OntIdTx Error,null codeHash");
    public static let ParamError = getError(58004,"OntIdTx Error,param error");
    public static let DidNull  = getError(58005,"OntIdTx Error,SendDid or receiverDid is null in metaData");
    public static let NotExistCliamIssuer = getError(58006,"OntIdTx Error,Not exist cliam issuer");
    public static let NotFoundPublicKeyId = getError(58007,"OntIdTx Error,not found PublicKeyId");
    public static let PublicKeyIdErr = getError(58008,"OntIdTx Error,PublicKeyId err");
    public static let BlockHeightNotMatch = getError(58009,"OntIdTx Error,BlockHeight not match");
    public static let BlockHeightLessThanZero = getError(58009,"OntIdTx Error,BlockHeight Less Than Zero");
    public static let NodesNotMatch = getError(58010,"OntIdTx Error,nodes not match");
    public static let ResultIsNull = getError(58011,"OntIdTx Error,result is null");
    public static let AssetNameError = getError(58012,"OntAsset Error,asset name error");
    public static let DidError = getError(58013,"OntAsset Error,Did error");
    public static let NullPkId = getError(58014,"OntAsset Error,null pkId");
    public static let NullClaimId = getError(58015,"OntAsset Error,null claimId");
    public static let CreateOntIdClaimErr = getError(58013,"OntIdTx Error, createOntIdClaim error");
    
    public static func ConstructedRootHashErr(_ msg:String) -> String{
    return getError(54003, "Other Error," + msg);
    }
    public static let NullKeyOrValue = getError(58016,"Record Error,null key or value");
    public static let NullKey = getError(58017,"Record Error,null  key");
    
    public static let GetAccountByAddressErr = getError(58018,"WalletManager Error,getAccountByAddress err");
    public static let WebsocketNotInit = getError(58019,"OntSdk Error,websocket not init");
    public static let ConnRestfulNotInit = getError(58020,"OntSdk Error,connRestful not init");
    
    //abi error
    public static let SetParamsValueValueNumError = getError(58021,"AbiFunction Error,setParamsValue value num error");
    public static let InvalidUrl = getError(58022,"Interfaces Error,Invalid url:");
    public static let AESailed = getError(58023,"ECIES Error,AES failed initialisation -");
    
    public static func OtherError(_ msg:String) -> String {
    return getError(59000, "Other Error," + msg);
    }
}
