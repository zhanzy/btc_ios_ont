//
//  UIntBase.swift
//  Base
//
//  Created by PC-269 on 2018/8/14.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

class UIntBase: Serializable {

    private var data_bytes:Data;
    
    init(_ bytes:Int, _ value:Data) {
        if (value.count == 0) {
            self.data_bytes = Data();
            return;
        }
        if (value.count != bytes) {
            //throw new SDKException(ErrorCode.ParamError);
            assert(true, "\(ErrorCode.ParamError)");
        }
        self.data_bytes = value;
    }
}
