//
//  Curve.swift
//  Base
//
//  Created by PC-269 on 2018/8/15.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

class Curve: NSObject {

    static let P224 = Curve(1, "P-224")
    static let P256 = Curve(2, "P-256")
    static let P384 = Curve(3, "P-384")
    static let P512 = Curve(4, "P-521")
    static let SM2P256V1 = Curve(20, "sm2p256v1")
    
    private var label:Int;
    private var name:String;
    
      init(_ v0:Int, _ v1:String) {
        
        self.label = v0;
        self.name = v1;
        super.init();
    }
    
    public func getLabel() -> Int {
        return label;
    }
    
    
    public func toString() -> String {
        return name;
    }
    
//     public class func valueOf(_ v:Curve) -> Curve {
//        for c in Curve.values() {
//            if (ECNamedCurveTable.getParameterSpec(c.toString()).getCurve().equals(v)) {
//                return c;
//            }
//        }
//    }
    
    public class func values() -> [Curve] {
        return [P224,P256,P384,P512,SM2P256V1];
    }
    
    public class func fromLabel(_ v:Int) -> Curve? {
        for c in Curve.values() {
            if (c.label == v) {
                return c;
            }
        }
        
        print("error! cure for label:\(v) is not exist!");
        return nil;
    }

}
