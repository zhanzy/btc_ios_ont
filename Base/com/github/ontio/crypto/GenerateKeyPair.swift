//
//  KeyPair.swift
//  Base
//
//  Created by PC-269 on 2018/8/16.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import EllipticCurveKeyPair

struct GenerateKeyPair {

    static let keyPair: EllipticCurveKeyPair.Manager = {
        let publicAccessControl = EllipticCurveKeyPair.AccessControl(protection: kSecAttrAccessibleAlwaysThisDeviceOnly, flags: [])
        let privateAccessControl = EllipticCurveKeyPair.AccessControl(protection: kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly, flags: [.userPresence, .privateKeyUsage])
        let config = EllipticCurveKeyPair.Config(
            publicLabel: "payment.sign.public",
            privateLabel: "payment.sign.private",
            operationPrompt: "Confirm payment",
            publicKeyAccessControl: publicAccessControl,
            privateKeyAccessControl: privateAccessControl,
            fallbackToKeychainIfSecureEnclaveIsNotAvailable: true)
        return EllipticCurveKeyPair.Manager(config: config)
    }()
}
