//
//  KeyType.swift
//  Base
//
//  Created by PC-269 on 2018/8/14.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

class KeyType:NSObject {

    static let ECDSA = 0x12,
    SM2 = 0x13,
    EDDSA = 0x14;
    
    
}
