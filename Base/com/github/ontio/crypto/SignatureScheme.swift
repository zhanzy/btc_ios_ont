//
//  SignatureScheme.swift
//  Base
//
//  Created by PC-269 on 2018/8/14.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import crypto
import secp256k1

public class SignatureScheme:NSObject {
    
    static let SHA224WITHECDSA = "SHA224withECDSA",
    SHA256WITHECDSA = "SHA256withECDSA",
    SHA384WITHECDSA = "SHA384withECDSA",
    SHA512WITHECDSA = "SHA512withECDSA",
    SHA3_224WITHECDSA = "SHA3-224withECDSA",
    SHA3_256WITHECDSA = "SHA3-256withECDSA",
    SHA3_384WITHECDSA = "SHA3-384withECDSA",
    SHA3_512WITHECDSA = "SHA3-512withECDSA",
    RIPEMD160WITHECDSA = "RIPEMD160withECDSA",
    SM3WITHSM2 = "SM3withSM2";
    
    init(_ name:String) {
        super.init();
        SignatureScheme(name);
    }
    
    public var name:String = "";
    private func SignatureScheme(_ v:String) {
        name = v;
    }

    public func toString() -> String {
        return name;
    }
}
