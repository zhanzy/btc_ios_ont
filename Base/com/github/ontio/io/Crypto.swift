//
//  Crypto.swift
//  BitcoinKit
//
//  Created by Kishikawa Katsumi on 2018/01/30.
//  Copyright © 2018 Kishikawa Katsumi. All rights reserved.
//

import Foundation
import crypto

//http://swift.gg/2016/10/12/swift3-nsdata-data/
public struct Crypto {
    public static func sha256(_ plain: Data) -> Data {
        let length = Int(SHA256_DIGEST_LENGTH)
        var result = [UInt8](repeating: 0, count: length)
        _ = plain.withUnsafeBytes { SHA256($0, plain.count, &result) }
        return Data(bytes: result, count: length)
    }
    
    public static func sha256sha256(_ plain: Data) -> Data {
        return sha256(sha256(plain))
    }

    public static func ripemd160(_ plain: Data) -> Data {
        let length = Int(RIPEMD160_DIGEST_LENGTH)
        var result = [UInt8](repeating: 0, count: length)
        _ = plain.withUnsafeBytes { RIPEMD160($0, plain.count, &result) }
        return Data(bytes: result, count: length)
    }

    public static func sha256ripemd160(_ plain: Data) -> Data {
        return ripemd160(sha256(plain))
    }

    public static func hmacsha512(key: Data, data: Data) -> Data {
        var length = UInt32(SHA512_DIGEST_LENGTH)
        var result = [UInt8](repeating: 0, count: Int(length))
        _ = key.withUnsafeBytes { (keyPtr) in
            data.withUnsafeBytes { (dataPtr) in
                HMAC(EVP_sha512(), keyPtr, Int32(key.count), dataPtr, data.count, &result, &length)
            }
        }
        
        return Data(result)
    }
    
    public static func hmacecdsa(key: Data, data: Data) -> Data {
        var length = UInt32(SHA512_DIGEST_LENGTH)
        var result = [UInt8](repeating: 0, count: Int(length))
        _ = key.withUnsafeBytes { (keyPtr) in
            data.withUnsafeBytes { (dataPtr) in
                HMAC(EVP_ecdsa(), keyPtr, Int32(key.count), dataPtr, data.count, &result, &length)
            }
        }
        return Data(result)
    }

}

public enum CryptoError : Error {
    case signFailed
    case noEnoughSpace
}
