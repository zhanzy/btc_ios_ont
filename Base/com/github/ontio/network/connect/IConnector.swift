//
//  IConnector.swift
//  Base
//
//  Created by PC-269 on 2018/8/13.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

public protocol IConnector:NSObjectProtocol {

      func getUrl() -> String;
//    Object sendRawTransaction(boolean preExec, String userid, String hexData) throws Exception;
//    Object sendRawTransaction(String hexData) throws Exception;
//    Transaction getRawTransaction(String txhash) throws Exception;
//    Object getRawTransactionJson(String txhash) throws Exception;
//    int getGenerateBlockTime() throws Exception;
//    int getNodeCount() throws Exception;
      func getBlockHeight() -> Int;
//    Block getBlock(int height) throws Exception;
//    Block getBlock(String hash) throws Exception ;
//    Object getBlockJson(int height) throws Exception;
//    Object getBlockJson(String hash) throws Exception;
//
//    Object getBalance(String address) throws Exception;
//
//    Object getContract(String hash) throws Exception;
//    Object getContractJson(String hash) throws Exception;
//    Object getSmartCodeEvent(int height) throws Exception;
//    Object getSmartCodeEvent(String hash) throws Exception;
//    int getBlockHeightByTxHash(String hash) throws Exception;
//
//    String getStorage(String codehash, String key) throws Exception;
//    Object getMerkleProof(String hash) throws Exception;
//    String getAllowance(String asset, String from, String to) throws Exception;
//    Object getMemPoolTxCount() throws Exception;
//    Object getMemPoolTxState(String hash) throws Exception;
}
