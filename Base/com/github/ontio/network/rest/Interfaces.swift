//
//  Interfaces.swift
//  Base
//
//  Created by PC-269 on 2018/8/10.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

class Interfaces: NSObject {
    
    private var url = "";
    
    init(_ url:String) {
        super.init()
        self.url = url;
    }
    
    public func getUrl() -> String {
        return url;
    }

    //http://polaris1.ont.io:20334/api/v1/block/height
    public func getBlockHeight() -> String {

        let params = Dictionary<String,String>();
        return http.get(url + UrlConsts.Url_get_block_height, params)
    }
}
