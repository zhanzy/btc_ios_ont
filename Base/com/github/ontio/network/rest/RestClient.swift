//
//  RestClient.swift
//  Base
//
//  Created by PC-269 on 2018/8/13.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

class RestClient: NSObject,IConnector {
    
    private var api:Interfaces!;
    
    init(_ restUrl:String) {
        super.init();
        api = Interfaces(restUrl)
    }
    
    
    //MARK: - IConnector
    func getUrl() -> String {
        return api.getUrl();
    }
    
    func getBlockHeight() -> Int {
        
//        String rs = api.getBlockHeight();
//        Result rr = JSON.parseObject(rs, Result.class);
//        if (rr.Error == 0) {
//            return (int) rr.Result;
//        }
//        throw new Exception(to(rr));
        let rs = api.getBlockHeight();
        let d = rs.convertToDictionary();
        if d == nil {
            print("error! d is error")
            return -1;
        }
        
        guard let c  = d!["Result"] else {
            print("error! erro is no exist!")
            return -1;
        }
        
        let result  = "\(c)";
        return Int(result)!;
    }
}
