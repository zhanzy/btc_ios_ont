//
//  urlrest.swift
//  Base
//
//  Created by PC-269 on 2018/8/10.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

public class UrlConsts {
    
    public static let Url_send_transaction = "/api/v1/transaction";
    public static let Url_get_transaction = "/api/v1/transaction/";
    public static let Url_get_generate_block_time = "/api/v1/node/generateblocktime";
    public static let Url_get_node_count = "/api/v1/node/connectioncount";
    public static let Url_get_block_height = "/api/v1/block/height";
    public static let Url_get_block_by_height = "/api/v1/block/details/height/";
    public static let Url_get_block_by_hash = "/api/v1/block/details/hash/";
    public static let Url_get_account_balance = "/api/v1/balance/";
    public static let Url_get_contract_state = "/api/v1/contract/";
    public static let Url_get_smartcodeevent_txs_by_height = "/api/v1/smartcode/event/transactions/";
    public static let Url_get_smartcodeevent_by_txhash = "/api/v1/smartcode/event/txhash/";
    public static let Url_get_block_height_by_txhash = "/api/v1/block/height/txhash/";
    public static let Url_get_storage = "/api/v1/storage/";
    public static let Url_get_merkleproof = "/api/v1/merkleproof/";
    public static let Url_get_mem_pool_tx_count = "/api/v1/mempool/txcount";
    public static let Url_get_mem_pool_tx_state = "/api/v1/mempool/txstate/";
    public static let Url_get_allowance = "/api/v1/allowance/";
}
