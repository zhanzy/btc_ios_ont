//
//  http.swift
//  Base
//
//  Created by PC-269 on 2018/8/10.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import SwiftyJSON
import Alamofire

class http: NSObject {
    
    private static var _instance: http! = {
        let ins = http();
        return ins;
    }()
    
    public class func getInstance() -> http! {
        return _instance;
    }
    
    //MARK:common
    class func bReturnStatus(dict:Dictionary<String,Any>?) -> Bool {
        
        if dict == nil {
            print("error! dict is not dict!");
            return false;
        }
        
        let d:Dictionary<String,Any> = dict!
        let status = d["code"] as? String
        if (status != nil) && Int(status!)! == 0 {
            return true;
        }
        
        return false;
    }
    
    class func log<T>(_ response:DataResponse<T>,_ params:[String:String]) {
        
        let baseUrl = response.request?.url?.absoluteString;
        
        var fullUrl = "";
        var arr:[String]! = [String]();
        for (key,value) in params {
            let str = key + "=" + value;
            arr.append(str);
        }
        if(arr.count > 0){
            let last  = arr.joined(separator: "&");
            fullUrl = baseUrl! + "?" + last;
        }
        print("fullUrl:\(fullUrl)");
        print("url:\(String(describing: baseUrl))\n params:\(params.json)")
        
        let json = JSON(response.data!)
        print("json:\(json)")
    }
    
    class func baseParams() -> Dictionary<String,Any> {
        return self.baseParams(bAZ: false);
    }
    
    class func baseParams(bAZ:Bool) -> Dictionary<String,Any> {
    
        let d = Dictionary<String,Any>();
        return d;
    }

    private class func  get(_ url:String,_ https:Bool) -> String {
        
//        self.loadData(url: url, path: "", MethodType.post) { (bSuccess, callBacks) in
//
//        }
    
        var request = URLRequest(url: URL(string: url)!)
        request.timeoutInterval = 50000
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        var result:Dictionary<String,Any> = Dictionary<String,Any>();
        let semaphore = DispatchSemaphore(value: 0)
        let session = URLSession.shared;
        let task = session.dataTask(with: request) { (data, response, error) in
            
            
            if  error != nil {
                print("error!\(String(describing: error?.localizedDescription))")
                semaphore.signal()
                return ;
            }
            
            let responsobject = try?JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments);
            print("response:\(responsobject ?? "kk")");
            if let d = responsobject as? Dictionary<String,Any> {
                result = d;
            }
            print("result:\(result)")
            semaphore.signal()
        }
        task.resume();
        
        semaphore.wait()
        return result.json;
    }
    
    class func  get(_ url:String, _ params:Dictionary<String,String>) -> String {
        
        if url.hasPrefix("https") {
            return get(url + cvtParams(params), true);
        }
        return get(url + cvtParams(params), false);
    }
    
    private static func cvtParams(_ params:Dictionary<String, String>?) -> String {
        
        if(params == nil || params!.isEmpty ){
            return "";
        }
        
        var last = "";
        for(k,v) in params! {
            if last.count == 0 {
                last = last + "\(k)=\(v)"
            }else {
                last = last + "&\(k)=\(v)"
            }
        }
        
        return  "?" + last;
    }
    
    class func loadData(path: String,_ type : MethodType, parameters : [String : String]? = nil, inView:UIView? = nil, finishedCallback : @escaping (_ bSuccess:Bool, _ result : Any) -> ()) {
        return http.loadData(url: severUrl, path: path, type, parameters: parameters, inView: inView, finishedCallback: finishedCallback);
    }
    
    //manual url
    class func loadData(url:String,path: String,_ type : MethodType, parameters : [String : String]? = nil, inView:UIView? = nil, finishedCallback : @escaping (_ bSuccess:Bool, _ result : Any) -> ()) {
        
        // 1.获取类型
        let method = type == .get ? HTTPMethod.get : HTTPMethod.post
        
        var params = http.baseParams() as! [String:String];
        if parameters != nil {
            params = params.merged(with: parameters!)
        }
        
        var URL = url;
        if path.count > 0 {
            URL = URL + "/" + path;
        }
        // 2.发送网络请求
        if(inView != nil){
            MBProgressHUD.showAdded(to: inView!, animated: true);
        }
        Alamofire.request(URL, method: method, parameters: params).responseJSON { (response) in
            
            http.log(response,params);
            
            if(inView != nil){
                MBProgressHUD.hide(for: inView!, animated: false);
            }
            // 3.获取结果
            guard let result = response.result.value else {
                print(response.result.error!)
                var d = [String:String]();
                d["msg"] = Const.msg.kServerStateErrorEx;
                finishedCallback(false,d);
                return
            }
            
            let last = result as! Dictionary<String,Any>;
            if self.bReturnStatus(dict: last) == false {
                
                var d = [String:Any]();
                d["msg"] = last["msg"] as? String;
                if last["code"] is NSNumber {
                    d["code"] = (last["code"] as! NSNumber).stringValue;
                }else {
                    d["code"] = last["code"];
                }
                finishedCallback(false,d);
                return;
            }
            
            // 4.将结果回调出去
            finishedCallback(true,last)
        }
    }
}

