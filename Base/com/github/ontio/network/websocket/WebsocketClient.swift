//
//  WebsocketClient.swift
//  Base
//
//  Created by PC-269 on 2018/8/13.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import Starscream

//https://github.com/daltoniam/Starscream
class WebsocketClient: NSObject {
    
    private var mWebSocket:WebSocket?;
    private var lock:Any? = nil;
    private var logFlag = false;
    private var reqId:Int = 0;
    public static var wsUrl = "";
    private static var wsClient:WebsocketClient? = nil;
    
    init(_ url:String, _ lock:Any) {
        super.init();
        
        WebsocketClient.wsUrl = url;
        self.lock = lock;
        WebsocketClient.wsClient = self;
    }
    
//    public void startWebsocketThread(boolean log) {
//    this.logFlag = log;
//    Thread thread = new Thread(
//    new Runnable() {
//    @Override
//    public void run() {
//    wsClient.wsStart();
//    }
//    });
//    thread.start();
//    }
    
    public func startWebsocketThread(_ log:Bool){
        
        let queue = DispatchQueue(label: "load");
        queue.async {
            self.wsStart();
        }
    }
    
    public func wsStart() {
    
        let url = WebsocketClient.wsUrl;
        var request = URLRequest(url: URL(string: url)!)
        //var request = URLRequest(url: URL(string: "ws://localhost:8080/")!)
        request.timeoutInterval = 5
//        request.setValue("someother protocols", forHTTPHeaderField: "Sec-WebSocket-Protocol")
//        request.setValue("14", forHTTPHeaderField: "Sec-WebSocket-Version")
//        request.setValue("Everything is Awesome!", forHTTPHeaderField: "My-Awesome-Header")
        request.setValue(url, forHTTPHeaderField: "Origin")
        let socket = WebSocket(request: request)
        //websocketDidConnect
        socket.onConnect = {
            print("websocket is connected")
        }
        //websocketDidDisconnect
        socket.onDisconnect = { (error: Error?) in
            print("websocket is disconnected: \(error?.localizedDescription ?? "")")
        }
        //websocketDidReceiveMessage
        socket.onText = { (text: String) in
            print("got some text: \(text)")
        }
        //websocketDidReceiveData
        socket.onData = { (data: Data) in
            print("got some data: \(data.count)")
        }
        //you could do onPong as well.
        socket.connect()
        mWebSocket = socket;
    }
    
    func generateReqId() -> Int {
        
        if reqId == 0 {
            return Int(arc4random());
        }

        return reqId
    }
    
    //MARK: - IConnect
    func getUrl() -> String {
        return WebsocketClient.wsUrl;
    }
    
//    public int getBlockHeight() throws Exception {
//    Map map = new HashMap<>();
//    map.put("Action", "getblockheight");
//    map.put("Version", "1.0.0");
//    map.put("Id", generateReqId());
//    mWebSocket.send(JSON.toJSONString(map));
//    return 0;
//    }
    func getBlockHeight() -> Int {
        
        var d = Dictionary<String,String>();
        d["Action"] = "getblockheight"
        d["Version"] = "1.0.0"
        d["Id"] = "\(generateReqId())"
        mWebSocket?.write(string: d.json, completion: {
            print("finished")
        })
        return 0;
    }

}

