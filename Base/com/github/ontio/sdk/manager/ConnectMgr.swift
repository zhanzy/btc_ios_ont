//
//  ConnectMgr.swift
//  Base
//
//  Created by PC-269 on 2018/8/13.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit


protocol IConnector {
    
    
    
    func getUrl() -> String;
    func getBlockHeight() -> Int;
}

class ConnectMgr:NSObject,IConnector {
    
    private var connector:IConnector!;
    
    func RestClient(_ url:String) -> IConnector {
        
        let c = ConnectMgr("","");
        return c;
    }
    
    init(_ url:String,_ type:String) {
        super.init();
        
        if type == "rpc" {
//            setConnector(RpcClient(url))
        } else if type == "restful" {
            setConnector(RestClient(url))
        }
    }
//    public ConnectMgr(String url, String type, Object lock) {
//    if (type.equals("websocket")){
//    setConnector(new WebsocketClient(url,lock));
//    }
//    }
//    public ConnectMgr(String url, String type) throws MalformedURLException {
//    if (type.equals("rpc")) {
//    setConnector(new RpcClient(url));
//    } else if (type.equals("restful")){
//    setConnector(new RestClient(url));
//    }
//    }
    
    func setConnector(_ connector:IConnector) {
        self.connector = connector;
    }
    
    //MARK: - IConnector
    func getUrl() -> String {
        
        return connector.getUrl();
    }
    
    func getBlockHeight() -> Int {
        
        return connector.getBlockHeight();
    }
    
}
