//
//  Account.swift
//  Base
//
//  Created by PC-269 on 2018/8/16.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

class Account: NSObject {

    public var label = "";
    public var address = "";
    public let isDefault = false;
    public let lock = false;
    public var algorithm = "";
    public var parameters = Dictionary<String,String>();
    public var key = "";
//    @JSONField(name = "enc-alg")
    public var encAlg = "aes-256-gcm";
    public var salt = "";
    public var hashEx = "sha256";
    public var publicKey = "";
    public var signatureScheme = "SHA256withECDSA";
    public var extra:String?;
    
    init(_ alg:String,_ params:[String],_ encAlg:String,_ scheme:String,_ hash:String){
        self.algorithm = alg;
        self.parameters["curve"] = params[0];
        self.signatureScheme = scheme;
        self.encAlg = encAlg;
        self.hashEx = hash;
        self.extra = nil;
        super.init()
    }
    
//    public Object getExtra(){
//    return extra;
//    }
//
//    public void setExtra(Object extra){
//    this.extra = extra;
//    }
//
//    public String getKey() {
//    return key;
//    }
//
//    public void setKey(String key) {
//    this.key = key;
//    }
//
//    public String getEncAlg(){
//    return encAlg;
//    }
//    public void setEncAlg(String encAlg){
//    this.encAlg = encAlg;
//    }
//    public String getHash(){
//    return hash;
//    }
//
//    public void setHash(String hash) {
//    this.hash = hash;
//    }
//
//    public byte[] getSalt() {
//    return Base64.decode(salt,Base64.NO_WRAP);
//    }
//
//    public void setSalt(byte[] salt) {
//    this.salt = new String(Base64.encode(salt,Base64.NO_WRAP));
//    }
//    public String getPublicKey(){
//    return publicKey;
//    }
//    public void setPublicKey(String pk){
//    this.publicKey = pk;
//    }
//    @Override
//    public String toString() {
//    return JSON.toJSONString(this);
//    }
}
