//
//  PrivateKey.swift
//  Base
//
//  Created by PC-269 on 2018/9/20.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import BitcoinKit

public struct PrivateKeyEx {
    public var  raw:Data
    
    init(_ seed:Data) {
        
        //let seed = "seed".data(using: .ascii)!
        var hmac:Data!;
        hmac = Crypto.hmacecdsa(key: "ONT seed".data(using: .ascii)!, data: seed)
        let privateKey = hmac[0..<32]
//        let chainCode = hmac[32..<64]
        self.raw = privateKey;
    }
    
    
    //通过私钥导出公钥
    public func publicKey() -> PublicKeyEx {
        return PublicKeyEx.init(self);
    }
    

}
