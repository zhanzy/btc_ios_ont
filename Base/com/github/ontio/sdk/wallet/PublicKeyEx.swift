//
//  PublicKey.swift
//  Base
//
//  Created by PC-269 on 2018/9/20.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit
import BitcoinKit

public struct PublicKeyEx {

    public var raw:Data
    public var addressU160:String
    
    init(_ privateKeyEx:PrivateKeyEx) {
        self.raw = PublicKey.from(privateKey: privateKeyEx.raw, compression: true)
        self.addressU160 = Crypto.sha256ripemd160(raw).hex;

    }
    
    public func toAddress() -> String {
//        let hash = Data([network.pubkeyhash]) + Crypto.sha256ripemd160(raw)
        let hash =  Crypto.sha256ripemd160(raw);
        return publicKeyHashToAddress(hash)
    }
    
}
