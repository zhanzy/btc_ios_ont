//
//  CommonExtension.swift
//  SNSBasePro
//
//  Created by zhan zhong yi on 2018/3/9.
//  Copyright © 2018年 zhan zhong yi. All rights reserved.
//

import UIKit

class CommonExtension: NSObject {
    
}

extension String {
    
        func fromBase64() -> String? {
            guard let data = Data(base64Encoded: self) else {
                return nil
            }
    
            return String(data: data, encoding: .utf8)
        }
    
        func toBase64() -> String {
            return Data(self.utf8).base64EncodedString()
        }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func toInt() -> Int {
        
        return Int(self)!;
    }
    
    var boolValue: Bool {
        return NSString(string: self).boolValue
    }
    
    public func intValue() -> Int {
        return Int(self)!;
    }
    
    
}


extension Data {
    var string: String? { return String(data: self, encoding: .utf8) }
}

extension Bool {
    
    func  toIntString() -> String {
        
        if self == true {
            return "1";
        }
        
        return "0";
    }
    
}


extension NSObject {
    
    public class var className: String {
        return NSStringFromClass(self).components(separatedBy: ".").last ?? ""
    }
    
    //    var getClass: AnyClass {
    //        let name = self.className;
    //        return NSClassFromString(name)!;
    //    }
    
    class var getClass: AnyClass {       // computed type property
        
        let name = self.className;
        return NSClassFromString(name)!;
    }
    
    public class func nib() -> UINib {
        return UINib.init(nibName: self.className, bundle: Bundle.main);
    }
    
    public class  func cellIdentifier() -> String {
        return self.className + "ID";
    }
    
    public class func nib3() -> UINib {
        return UINib.init(nibName: self.className + "3", bundle: Bundle.main);
    }
    
    public class  func cellIdentifier3() -> String {
        return self.className + "3ID";
    }
    
}

extension NSNumber {
    
    open func length() -> NSInteger {
        return 0;
    }
}

extension NSNull {
    
    //    -(int)intValue;
    //    -(NSInteger)integerValue;
    //    -(NSInteger)length;
    //    -(CGFloat)floatValue;
    
    
    //    void dynamicMethodIMP(id self, SEL _cmd) {
    //    // implementation ....
    //    NSString *cmd = NSStringFromSelector(_cmd);
    //    //#ifdef DEBUG
    //    //    NSAssert(0, @"[NSNull %@]: unrecognized selector ",cmd);
    //    //#endif
    //    }
    //
    //    + (BOOL)resolveInstanceMethod:(SEL)aSEL
    //    {
    //    if (aSEL == @selector(intValue) || aSEL == @selector(integerValue) || aSEL == @selector(length) || aSEL == @selector(floatValue)) {
    //    class_addMethod([self class], aSEL, (IMP) dynamicMethodIMP, "v@:");
    //    return YES;
    //    }
    //
    //    return [super resolveInstanceMethod:aSEL];
    //    }
    //    void dynamicMethodIMP(id self, SEL _cmd) {
    //    // implementation ....
    //    NSString *cmd = NSStringFromSelector(_cmd);
    //    //#ifdef DEBUG
    //    //    NSAssert(0, @"[NSNull %@]: unrecognized selector ",cmd);
    //    //#endif
    //    }
    
    @objc open func intValue() -> Int {
        
        return 0;
    }
    
    @objc open  func integerValue() -> NSInteger {
        return 0;
    }
    
    @objc open func length() -> NSInteger {
        return 0;
    }
    
    @objc open func floatValue() -> CGFloat {
        return 0;
    }
    
    @objc func dynamicMethodIMP(target:Any,_cmd:Selector) -> Void {
        //        let cmd = _cmd.description;
    }
    
    //    open override static func initialize() {
    //        // Method Swizzling
    //    }
    
    // 1
    override open class func resolveInstanceMethod(_ sel: Selector!)
        -> Bool {
            // 添加实例方法并返回 true 的一次机会，它随后会再次尝试发送消息
            if sel ==  #selector(self.intValue) || sel ==  #selector(self.integerValue) || sel ==  #selector(self.length) || sel ==  #selector(self.floatValue) {
                
                if Const.bDebug == true {
                    assert(false, String.init(format: "[NSNull \(sel.description)] : unrecognized selector "));
                }
                
                let swizzledSelector = #selector(NSNull.dynamicMethodIMP(target:_cmd:))
                let swizzledMethod = class_getInstanceMethod(self,swizzledSelector);
                class_addMethod(self, sel, method_getImplementation(swizzledMethod!), method_getTypeEncoding(swizzledMethod!));
                
                return true;
            }
            
            return false;
    }
    
    //    // 2
    //    override func forwardingTarget(for aSelector: Selector!) ->
    //        Any? {
    //            // 返回可以处理 Selector 的对象
    //    }
}


extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
    
    mutating func merge(with dictionary: Dictionary) {
        dictionary.forEach { updateValue($1, forKey: $0) }
    }
    
    func merged(with dictionary: Dictionary) -> Dictionary {
        var dict = self
        dict.merge(with: dictionary)
        return dict
    }
    
    public static func += (lhs: inout [Key: Value], rhs: [Key: Value]) {
        rhs.forEach({ lhs[$0] = $1})
    }
    
    //    public static func += <K, V> (left: inout [K:V], right: [K:V]) {
    //        for (k, v) in right {
    //            left[k] = v
    //        }
    //    }
    
    //    static func += <K,V>(left: [K: V], right: [K: V])
    //        -> [K: V]
    //    {
    //        var map = [K: V]()
    //        for (k, v) in left {
    //            map[k] = v
    //        }
    //        for (k, v) in right {
    //            map[k] = v
    //        }
    //        return map
    //    }
    
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    
    func printJson() {
        print(json)
    }
}


//swift 下的runtime 必须基本 nsobject 类
//解释得比较清楚的文章
//extension UIViewController {
//
//    open override static func initialize() {
//        struct Static {
//            static var token = NSUUID().uuidString
//        }
//
//        if self != UIViewController.self {
//            return
//        }
//
//        DispatchQueue.once(token: Static.token) {
//            let originalSelector = #selector(UIViewController.viewWillAppear(_:))
//            let swizzledSelector = #selector(UIViewController.xl_viewWillAppear(animated:))
//
//            let originalMethod = class_getInstanceMethod(self, originalSelector)
//            let swizzledMethod = class_getInstanceMethod(self, swizzledSelector)
//
//
//            //在进行 Swizzling 的时候,需要用 class_addMethod 先进行判断一下原有类中是否有要替换方法的实现
//            let didAddMethod: Bool = class_addMethod(self, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
//            //如果 class_addMethod 返回 yes,说明当前类中没有要替换方法的实现,所以需要在父类中查找,这时候就用到 method_getImplemetation 去获取 class_getInstanceMethod 里面的方法实现,然后再进行 class_replaceMethod 来实现 Swizzing
//
//            if didAddMethod {
//                class_replaceMethod(self, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
//            } else {
//                method_exchangeImplementations(originalMethod, swizzledMethod)
//            }
//        }
//    }
//
//    func xl_viewWillAppear(animated: Bool) {
//        self.xl_viewWillAppear(animated: animated)
//        print("xl_viewWillAppear in swizzleMethod")
//    }
//}

//用于swift下的runtime 只执行一次
public extension DispatchQueue {
    private static var onceTracker = [String]()
    
    public class func once(token: String, block:() -> Void) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        if onceTracker.contains(token) {
            return
        }
        
        onceTracker.append(token)
        block()
    }
}

@IBDesignable
class TextField: UITextField {
    @IBInspectable var insetX: CGFloat = 0
    @IBInspectable var insetY: CGFloat = 0
    
    // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
}

