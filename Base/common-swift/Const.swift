//
//  Const.swift
//  LocationU
//
//  Created by zhan zhong yi on 17/1/4.
//  Copyright © 2017年 zhan zhong yi. All rights reserved.
//

import UIKit
import Darwin;

class Const: NSObject {

    static let bDebug = false;
    static let bUseAliPay = false;
    static let bUseStoreKitClear = true;
    static let kXieHouMsgUpadte = "kXieHouMsgUpadte";
    static let isPhoneX_Add_Height_Ex  = 34;
    static let kAppID   =        "1178548652"
    static let AppIDNearBy_Ex =  "1178548652"
    static let AppLinkNearBy_Ex = String.init(format: "https://itunes.apple.com/app/id%@", Const.AppIDNearBy_Ex)
    static let Default_VPN_Account =  "Default_VPN_Account"
    
    static let kBundleIdEx =  "com.bigpang.tor"
    static let kInappRemoveAdsnoEx  = kBundleIdEx + ".removeAdsno";
    static let kInappRemoveAdsEx =  kBundleIdEx + ".removeAdsno";
    static let kInappDeleteItemIdEx =  kBundleIdEx + ".buyitemconsume";
    static let kInappItem1Ex  = kBundleIdEx + ".sub.0.99";
    static let kInappItem4Ex  = kBundleIdEx + ".sub.1.99";
    static let kInappItem8Ex  = kBundleIdEx + ".sub.3.99";
    static let kInappItem100Ex = kBundleIdEx + ".sub.7.99";
    
    struct url {
        
        static let ServerCheckUrlEx = ""
        static let kTDDomainEx = ""
        static let kTDDomain_SNS_Ex = ""
        
        static let kTDGeneralURLEx = kTDDomainEx + "/clpig";
        static let kTDGeneralURL_SNS_Ex = kTDDomain_SNS_Ex + "/news";
        
        static let kTDDomainOrgEx = ""
        static let kTDGeneralURLOrgEx = kTDDomainOrgEx + "/clpig"; //get recommends, 获取推荐的app
        static let kTDGeneralURLUploadTokenEx = kTDDomain_SNS_Ex + "/news"; //upload token //上传token 及用户
        
        static let kTDDomain_AZ_Ex = ""
        static let kTDDomain_SNS_AZ_Ex = ""
        static let kTDGeneralURL_AZ_Ex = kTDDomain_AZ_Ex + "/pm";
        static let kTDGeneralURLOrg_AZ_EX = kTDDomain_AZ_Ex + "/clpig";
        static let kTDGeneralURL_SNS_AZ_Ex = kTDDomain_AZ_Ex + "/pm/";
        static let kTDGeneralURLUploadToken_AZ_Ex = kTDDomain_AZ_Ex + "/pm";
        
        static let kTDGeneralURL_SNS_News_Ex = kTDDomainEx + "/news_look_for";
        
        //nearby
        static let NearBy_SNS_Ex = true;
        static let kTDDomain_nb_Ex = "" //@"117.143.221.190"
        static let kTDGeneralURL_nb_Ex = kTDDomain_nb_Ex + "/nearby"
        static let kTDGeneralURLUploadToken_nb_Ex =  kTDDomain_nb_Ex + "/nearby"
        static let kTDGeneralURLPay_Ex = kTDDomain_nb_Ex + "/nearby"
        
        static let PhotoListUrlEx = "" //美图搜索接
        static let VideoListUrlEx = ""     //搜狗视频
        static let BingSearchUrlEx = ""; //必应网盘搜索接口
    }
    
    struct msg {
    
        static let print_default = "打印信息出错！"
        static let kServerStateErrorEx = "服务器状态异常！"
    }
    
    struct value {
        static let kSectionHeight = 10
    }

    func RGBCOLOR(r:CGFloat,_ g:CGFloat,_ b:CGFloat) -> UIColor
    {
        return UIColor(red: (r)/255.0, green: (g)/255.0, blue: (b)/255.0, alpha: 1.0)
    }
    
    struct GlobalConstants {
        //  Device IPHONE
        //   Constant Variable.
        static let kBirthDate                     =    "DateOfBirth"
        static let kFirstName                     =    "FirstName"
        static let kLastName                      =    "LastName"
    }
    
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let IS_IPHONE_P_OR_MORE  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 736.0
    }
    
    let IS_IPAD = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad
    let IS_IPHONE = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone
    struct DeviceType
    {
        static let IS_IPHONE            = UIDevice.current.userInterfaceIdiom == .phone
        static let IS_IPHONE_4_OR_LESS  = IS_IPHONE && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_7          = IS_IPHONE_6
        static let IS_IPHONE_7P         = IS_IPHONE_6P
        static let isPhoneX             = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
        static let IS_IPAD_PRO_9_7      = IS_IPAD
        static let IS_IPAD_PRO_12_9     = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
        
        let IS_IPHONE_4_OR_LESS = (IS_IPHONE && UIScreen.main.bounds.size.height < 568.0)
        let IS_IPHONE_5 = (IS_IPHONE && UIScreen.main.bounds.size.height == 568.0)
        let IS_IPHONE_6 = (IS_IPHONE && UIScreen.main.bounds.size.height == 667.0)
        let IS_IPHONE_6P = (IS_IPHONE && UIScreen.main.bounds.size.height == 736.0)
    }
    
    struct Version{
        static let SYS_VERSION_FLOAT = (UIDevice.current.systemVersion as NSString).floatValue
        static let iOS7 = (Version.SYS_VERSION_FLOAT < 8.0 && Version.SYS_VERSION_FLOAT >= 7.0)
        static let iOS8 = (Version.SYS_VERSION_FLOAT >= 8.0 && Version.SYS_VERSION_FLOAT < 9.0)
        static let iOS9 = (Version.SYS_VERSION_FLOAT >= 9.0 && Version.SYS_VERSION_FLOAT < 10.0)
        static let iOS10 = (Version.SYS_VERSION_FLOAT >= 10.0 && Version.SYS_VERSION_FLOAT < 11.0)
        
        //MARK: System Version
        let IS_OS_7_OR_LATER = Version.SYS_VERSION_FLOAT >= 7.0
        let IS_OS_8_OR_LATER = Version.SYS_VERSION_FLOAT >= 8.0
        let IS_OS_9_OR_LATER = Version.SYS_VERSION_FLOAT >= 9.0
    }
    
    
    static let SCREEN_BOUNDS = UIScreen.main.bounds
    static let SCREEN_SCALE = UIScreen.main.bounds
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    
    struct font {
        
        static let kAPPFont120_Bold  = UIFont.boldSystemFont(ofSize: 60);
        static let kAPPFont36_Bold  = UIFont.boldSystemFont(ofSize: 18);
        static let kAPPFont36 = UIFont.systemFont(ofSize: 18.0);
        static let kAPPFont32 = UIFont.systemFont(ofSize: 16.0);
        static let kAPPFont30 = UIFont.systemFont(ofSize: 15.0);
        static let kAPPFont24 = UIFont.systemFont(ofSize: 12.0);
        static let kAPPFont20 = UIFont.systemFont(ofSize: 10.0);
        static let kAPPFont18 = UIFont.systemFont(ofSize: 9.0);
        static let kAPPFont16 = UIFont.systemFont(ofSize: 8.0);
    }
    
    struct color {
        
        static let kAPPDefaultBorderColor = ZYUtilsSW.getColor(hexColor: "c7c8cc");
        static let kAPPDefaultBgColor = ZYUtilsSW.getColor(hexColor: "f8f8f8");
        static let kAPPDefaultBgLightGrayColor = ZYUtilsSW.getColor(hexColor: "f3f3f3");
        static let kAPPDefaultBlackColor = ZYUtilsSW.getColor(hexColor: "2f2f2f");
        static let kAPPDefaultGrayColor = ZYUtilsSW.getColor(hexColor: "898989");
        static let kAPPDefaultLightGrayColor = ZYUtilsSW.getColor(hexColor: "cccccc");
        static let kAPPDefaultWhiteColor = ZYUtilsSW.getColor(hexColor: "ffffff");
        static let kAPPDefaultRedColor = ZYUtilsSW.getColor(hexColor: "fa4c4e");
        static let kAPPDefaultNavBarTextColor = ZYUtilsSW.getColor(hexColor: "ffffff");
        static let kAPPDefaultNavBarBgColor = ZYUtilsSW.getColor(hexColor: "fa414b");
        static let kAPPDefaultTabBarTextColor = ZYUtilsSW.getColor(hexColor: "838383");
        static let kAPPDefaultTabBarTextSeletColor = ZYUtilsSW.getColor(hexColor: "973ae2");
        static let kAPPDefaultTabBarBgColor = ZYUtilsSW.getColor(hexColor: "ffffff");
        static let kAPPDefaultBlackExColor = ZYUtilsSW.getColor(hexColor: "2f2f2f");
        static let kAPPDefaultGoldColor = ZYUtilsSW.getColor(hexColor: "fee82f");
        static let kAPPDefaultLineColor = ZYUtilsSW.getColor(hexColor: "e9e9e9");
        static let kAPPDefaultBottleBlueColor = ZYUtilsSW.getColor(hexColor: "4ed6f7");
        static let kAPPDefaultVipColor = ZYUtilsSW.getColor(hexColor: "fa4c4e");
    }
    
    class func easelocalize(key:String,value:String) -> String {
        
        let url = Bundle.main.url(forResource: "EaseUIResource", withExtension: "bundle")
        let str = Bundle.init(url: url!)?.localizedString(forKey:key, value: value, table: nil);
        return str!;
    }
    
    struct Formatters {
        static let _temperatureFormatter: NumberFormatter = {
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.maximumFractionDigits = 1
            
            return formatter
        }()
        
        static func temperatureFormatter(temperature: NSNumber) -> String? {
            return _temperatureFormatter.string(from: temperature)
        }
    }
}

