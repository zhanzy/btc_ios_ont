//
//  CustomNSError.swift
//  SNSBasePro
//
//  Created by zhan zhong yi on 2017/9/7.
//  Copyright © 2017年 zhan zhong yi. All rights reserved.
//

import UIKit

public protocol CustomNSError: Error {
    
    /// The domain of the error.
     static var errorDomain: String { get }
    
    /// The error code within the given domain.
    var errorCode: Int { get }
    
    /// The user-info dictionary.
    var errorUserInfo: [String : Any] { get }
}
