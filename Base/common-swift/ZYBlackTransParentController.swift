//
//  ZYBlackTransParentController.swift
//  SNSBasePro
//
//  Created by zhan zhong yi on 2017/9/5.
//  Copyright © 2017年 zhan zhong yi. All rights reserved.
//



import UIKit
import Foundation;


class ZYBlackTransParentController: UIViewController {
    
    @IBOutlet weak var _imageView: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.UISetUp();
    }
    
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated);
        
    }
    
    override func viewWillDisappear(_ animated:Bool){
        super.viewWillDisappear(animated);
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //common
     func UISetUp(){
        
        self.view.backgroundColor =  UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        let sampleTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(recognizer:)))
        _imageView?.addGestureRecognizer(sampleTapGesture)
        _imageView?.isUserInteractionEnabled = true;
    
//        //self.title = "";
//        let item = UIBarButtonItem().bk_init(with:UIImage(named: "more_nb_icon"), style: UIBarButtonItemStyle.plain, handler: {
//            (sender) in
//    
//            self.leftBarItemClicked(sender: (Any).self);
//        });
//        self.navigationItem.leftBarButtonItem = item as? UIBarButtonItem;
//        
//         let item1 = UIBarButtonItem().bk_init(with:UIImage(named: "more_nb_icon"), style: UIBarButtonItemStyle.plain, handler: {
//            (sender) in
//            
//            self.rightBarItemClicked(sender:(Any).self);
//        });
//        self.navigationItem.rightBarButtonItem = item1 as? UIBarButtonItem;
//    
//        self.loadDataWithPage(page: 1, inView: self.view) {
//            (bSuccess, callBacks) in
//            print("123");
//            //let mod:BaseModel? = callBacks as? BaseModel;
//            print("loadData success ",callBacks.debugDescription);
//        }
        
    }
    
    @objc public func backGroudViewTap(sender:Any?) {
        
    }
    
    @objc func tap(recognizer: UITapGestureRecognizer) {
        print("Tapping working")
        
        if self.responds(to: #selector(backGroudViewTap(sender:))) == true {
            self.backGroudViewTap(sender: self);
            return;
        }
        
        self.dismiss(animated: false, completion: nil)
    }

}
